#!/bin/bash

# 定义彩色日志函数
function log() {
  local color=$1
  shift
  echo -e "\033[${color}m$@\033[0m"
}

# 从命令行参数获取protoName
protoName=$1

# 检查protoName是否为空
if [ -z "$protoName" ]; then
  log 31 "Error: protoName is empty."
  exit 1
fi

# 在当前目录下创建一个名为protoName的目录
if ! mkdir $protoName; then
  log 31 "Error: failed to create directory $protoName."
  exit 1
fi

# 进入该目录
if ! cd $protoName; then
  log 31 "Error: failed to enter directory $protoName."
  exit 1
fi

# 生成proto文件
if [ ! -f $protoName.proto ]; then
    if ! echo "syntax = \"proto3\";" > $protoName.proto; then
      log 31 "Error: failed to create proto file."
      exit 1
    fi

#    if ! echo "package zrpc.brokenews.$protoName;" >> $protoName.proto; then
#      log 31 "Error: failed to write to proto file."
#      exit 1
#    fi
#
#    if ! echo "option go_package = \"gitee.com/brokenews/protocol/$protoName\";" >> $protoName.proto; then
#      log 31 "Error: failed to write to proto file."
#      exit 1
#    fi
    if ! echo "package pb;" >> $protoName.proto; then
      log 31 "Error: failed to write to proto file."
      exit 1
    fi

    if ! echo "option go_package = \"./pb\";" >> $protoName.proto; then
      log 31 "Error: failed to write to proto file."
      exit 1
    fi

    if ! echo "message DemoReq {};message DemoResp{};" >> $protoName.proto; then
      log 31 "Error: failed to write to proto file."
      exit 1
    fi

    if ! echo "service DemoService {rpc Demo(DemoReq)returns(DemoResp);}" >> $protoName.proto; then
      log 31 "Error: failed to write to proto file."
      exit 1
    fi
fi

# 在当前目录执行go mod init
if ! go mod init "gitee.com/brokenews/protocol/$protoName"; then
  log 31 "Error: failed to initialize go module."
  exit 1
fi

# 执行成功，输出日志
log 32 "Success: created directory $protoName and generated proto file."
log 32 "you can sh build.sh $protoName and generated proto file."