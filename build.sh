#!/bin/bash

# 定义彩色日志函数
function log() {
  local color=$1
  shift
  echo -e "\033[${color}m$@\033[0m"
}

# 从命令行参数获取protoName
protoName=$1
isMock=$2

# 检查protoName是否为空
if [ -z "$protoName" ]; then
  log 31 "Error: protoName is empty."
  exit 1
fi

# 检查protoName目录是否存在
if [ ! -d "$protoName" ]; then
  log 31 "Error: Directory $protoName does not exist."
  exit 1
fi

# 进入该目录
cd $protoName

# 执行protoc命令
if ! protoc --go_out=. --go-grpc_out=. *.proto;  then
  log 31 "Error: failed to execute protoc command."
  exit 1
fi

# 递归进入gitee.com目录
if [ -d "pb" ]; then
  cd pb

  # 找到go文件并移动到gitee.com同级目录下
  find . -name "*.go" -exec mv {} .. \;

  # 返回到上一级目录并删除gitee.com目录
  cd ..
  rm -rf pb
else
  log 31 "Error: Directory gitee.com does not exist."
  exit 1
fi

  if [ "${isMock}" == 1 ]; then
      mockgen -source="${protoName}_grpc.pb.go" -destination="${protoName}_mock.go" -package=$protoName
  fi
  go mod tidy
# 执行成功，输出日志
log 32 "Success: executed protoc command and moved go files."